# Cyberpunk77Autocraft

This is an AutoHotkey script meant for idle crafting.

**Requirements**
- You should have Cyberpunk 2077 updated to at least patch 1.05.

**Usage**
- Launch it, you should hear a couple of BEEPS.
- Go in-game and hold your mouse over the CRAFT button.
- Press F6 to initialize the idle crafting.
- Press F6 again to pause it, you should hear a single BEEP.
- Press F6 to resume the idle crafting again.
- To close the program press F7, you should hear a couple of BEEPS.

**Download**
- [Cyberpunk77Autocraft.exe](/uploads/328499d727c11caa18c7d23cae690cc4/Cyberpunk77Autocraft.exe) **v1.0**
