#SingleInstance Force
#MaxThreadsPerHotkey 2
#NoTrayIcon

Toggle := !Toggle ; Dumb boolean declaration, this Init Toggle to true

SOUNDBEEP, 750, 300
SLEEP, 200
SOUNDBEEP, 750, 300

F6::
    Toggle := !Toggle
    if !Toggle
    {
        loop
        {
            if !Toggle
            {
                SEND, {LButton Down}
                SLEEP, 1100
                SEND, {LButton Up}
                SLEEP, 30
             }
            else
            {
                SEND, {LButton Up}
                SOUNDBEEP, 750, 500
                break
            }
        }
    }
return

F7::
    SOUNDBEEP, 750, 300
    SLEEP, 200
    SOUNDBEEP, 750, 300
    EXITAPP
return
